#!/bin/bash
# .bash_aliases

# User specific aliases and functions
export PATH=$HOME/bin:$PATH
export PATH=$HOME/bin/java/bin:$PATH
export PATH=$HOME/bin/android-sdk-linux_86/tools:$PATH
export PATH=$PATH:$HOME/bin/eclipse
export EDITOR='emacs -nw --no-desktop'
export GIT_EDITOR=$EDITOR
export BC_ENV_ARGS=~/.bcrc
export TODOTXT_DEFAULT_ACTION=ls

alias ls='ls --color=auto'
alias ...='cd ../..'
alias ..='cd ..'
alias l='ls -lahF'
alias l1='ls -1'
alias e='emacs -nw'
alias grep='grep --color=auto'
alias words='echo $(cat *.txt | wc -w)'
alias pages='echo $((`words` / 250))'
alias t='todo.sh -t'
alias tw='todo.sh ls @work'
alias th='todo.sh ls @home'
alias td='todo.sh do'
alias ta='t add'
alias daily='e ~/Dropbox/Projects/Writing/Misc/`date -I`.txt'

# start the ssh-agent in the background
eval "$(ssh-agent -s)"
# ssh-add ~/.ssh/id_rsa

# Git prompt
source ~/.git-prompt.sh

PS1='[\u@\h \W$(__git_ps1 " (%s)")]\$ '

# Work Stuff
# export WORKSPACE=work
# export QTDIR=/usr/share/qt3
# export QTDIR=/usr/share/qt4
# PYTHONPATH=$HOME/.python
# export PATH=$PATH:/opt/freescale/usr/local/gcc-4.5.55-eglibc-2.11.55/powerpc-linux-gnu/bin/
# export PATH=$PATH:$QTDIR/bin
# export PATH=$PATH:/opt/eldk-5.3/powerpc-e500v2/sysroots/i686-eldk-linux/usr/bin/ppce500v2-linux-gnuspe


