(require 'package)
(package-initialize)
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)

;;(add-to-list 'load-path "~/.emacs.d/plugins/selectric-mode")
;;(require 'selectric-mode)

(column-number-mode 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Color text like a madman
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Turn on font-lock in all modes that support it
(if (fboundp 'global-font-lock-mode)
    (global-font-lock-mode t))

;; Maximum colors
(setq font-lock-maximum-decoration t)

;; Tis better to be lazy 
;;(setq font-lock-support-mode 'lazy-lock-mode)

;; And set the bg black and the fg green by default
(set-foreground-color "green")
(set-background-color "black")

;; Things I like to have defined

;; set the scratch text
(setq initial-scratch-message ";; Start by doing what's necessary,\n;; then do what's possible,\n;; and suddenly you are doing the impossible. -- Francis of Assisi\n\n")

;; disable the splash screen
(setq inhibit-splash-screen t)

;; get back a nice, fat cursor
(setq bar-cursor nil)

(add-to-list 'auto-mode-alist '("\\.html?\\'" . html-mode))

;; Set AJA standard Tab Width and style
(setq-default tab-width 4) 
(setq-default indent-tabs-mode nil)
(setq c-default-style "linux"
          c-basic-offset 4)
;; setup extra GDB bindings
;;(global-set-key "[f5]" 'gdb)

;; setup extra compile bindings 
(global-set-key [f6] 'compile)
(global-set-key [f7] 'next-error)

;; Set AJA ENV
(setenv "NTV2TARGET" "XENA2")

;; load a cool whitespace highlighting function
;;(require 'show-wspace)

;; make a quick HTML template for a short story or novel
(fset 'make-story-template
   [?< ?H ?T ?M ?L ?> return ?< ?H ?E ?A ?D ?> ?< ?T ?I ?T ?L ?E ?> ?< ?/ ?T ?I ?T ?L ?E ?> ?< ?/ ?H ?E ?A ?D ?> return ?< ?B ?O ?D ?Y ?> return ?< ?> left ?/ ?B ?O ?D ?Y right return ?< ?/ ?H ?T ?M ?L ?> up up up right right right right right right down down up return ?< ?C ?E ?N ?T ?E ?R ?> ?< ?> left ?B ?R right ?< ?B ?R ?> ?b ?y ?  ?B ?i ?l ?l ?  ?G ?l ?o ?v ?e ?r ?< ?B ?R ?> ?< ?/ ?C ?E ?N ?T ?E ?R ?> return ?< ?P ?> ?< ?/ ?P ?> ?\C-a ?\C-k ?\C-y up up up right right right right right right])

;;kill whitespace
(defun kill-whitespace ()
   "Kill the whitespace between two non-whitespace characters"
   (interactive "*")
   (save-excursion
     (save-restriction
       (save-match-data
  (progn
    (re-search-backward "[^ \t\r\n]" nil t)
    (re-search-forward "[ \t\r\n]+" nil t)
    (replace-match " " nil nil))))))

(defun join-line ()
  "Join the next line to this one with a single space between"
  (interactive "*")
  (end-of-line )
  (kill-whitespace ))

(global-set-key "\C-\M-j" 'join-line)


(defun previous-window ()
   (interactive "*")
  (other-window -1)
)

(global-set-key "\C-xp" 'previous-window)

;; count the words in a region
;; from the emacs-lisp-intro
(defun count-words-region (beginning end)
  "Print number of words in the region."
  (interactive "r")
  (message "Counting words in region ... ")
  
  ;;; 1. Set up appropriate conditions.
  (save-excursion
	(let ((count 0))
	  (goto-char beginning)
	  
     ;;; 2. Run the while loop.
	  (while (and (< (point) end)
				  (re-search-forward "\\w+\\W*" end t))
		(setq count (1+ count)))
	  
     ;;; 3. Send a message to the user.
	  (cond ((zerop count)
			 (message
			  "The region does NOT have any words."))
			((= 1 count)
			 (message
			  "The region has 1 word."))
			(t
			 (message
			  "The region has %d words." count))))))

(global-set-key "\C-xw" 'count-words-region)
 
(defun timestamp ()
  (interactive)
  (insert (format-time-string "%Y%m%d")))

(global-set-key "\M-m" 'timestamp)

;; These cause problems in headless Ubuntu shells
;; (setq x-select-enable-clipboard t)
;; (setq interprogram-paste-function 'x-cut-buffer-or-selection-value)

;; Bind c++-mode to all .h files
(setq auto-mode-alist
      (append (list (cons "\\.h" 'c++-mode))
                auto-mode-alist))

;; Bind lua-mode to all specification files.
(setq auto-mode-alist
      (append (list (cons "\\.specification" 'lua-mode))
                auto-mode-alist))

(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

;; Load CEDET
;;(load-file "~/lisp/cedet/common/cedet.el")

;;(add-to-list 'load-path "~/lisp/ecb")
;;(require 'ecb)

;; Enabling various SEMANTIC minor modes.  See semantic/INSTALL for more ideas.
;; Select one of the following
;;(semantic-load-enable-code-helpers)
;;(semantic-load-enable-guady-code-helpers)
;;(semantic-load-enable-excessive-code-helpers)

;;(setq semantic-load-turn-everything-on t)
;;(global-semantic-show-dirty-mode -1)
;;(global-semantic-show-unmatched-syntax-mode -1)

;;(setq semantic-complete-analyze-inline t)
;;(setq global-semantic-idle-completions-mode t)

;;(setq semanticdb-project-roots
;;         (list "/home/bglover/work/ntv2projects"))

;; TADs is an adventure game programming language.
;; This mode is heavil based on C moded.

;;(autoload 'tads2-mode "tads2-mode" "TADS 2 editing mode." t)
;;(setq auto-mode-alist
;;      (append (list (cons "\\.t$" 'tads2-mode))
;;                auto-mode-alist))

;;Working Python support (Emacs 22 has a built in mode that's broken
;;but Python provides a better one)
;;(autoload 'python-mode "python-mode" "Python Mode." t)
;;(add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))
;;Include AJA tests
;;(add-to-list 'auto-mode-alist '("\\.aja\\'" . python-mode))
;;(add-to-list 'interpreter-mode-alist '("python" . python-mode))

;;(add-hook 'python-mode-hook
;;		  (lambda ()
;;			(set (make-variable-buffer-local 'beginning-of-defun-function)
;;				 'py-beginning-of-def-or-class)
;;			(setq outline-regexp "def\\|class ")))

; (autoload 'visible-whitespace-mode "~/.emacs.d/visws.el"
;  "Visible Whitespace Mode." t)

;; Put autosave files (ie #foo#) in one place, *not*
;; scattered all over the file system!
(defvar autosave-dir "~/.emacs.autosaves/")

(make-directory autosave-dir t)

(defun auto-save-file-name-p (filename)
  (string-match "^#.*#$" (file-name-nondirectory filename)))

(defun make-auto-save-file-name ()
  (concat autosave-dir
   (if buffer-file-name
      (concat "#" (file-name-nondirectory buffer-file-name) "#")
    (expand-file-name
     (concat "#%" (buffer-name) "#")))))

;; Put backup files (ie foo~) in one place too. (The backup-directory-alist
;; list contains regexp=>directory mappings; filenames matching a regexp are
;; backed up in the corresponding directory. Emacs will mkdir it if necessary.)
(defvar backup-dir autosave-dir)
(setq backup-directory-alist (list (cons "." backup-dir)))

(desktop-save-mode 1)


;; (if (file-exists-p "/usr/share/emacs/site-lisp/emacs-jabber") 
;; 	;; adjust this path:
;; 	(add-to-list 'load-path "/usr/share/emacs/site-lisp/emacs-jabber")
;;   ;; For 0.7.90 and above:
;;   (require 'jabber-autoloads)
;; )

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(ecb-options-version "2.32")
  ;; '(jabber-account-list (quote (("billg@aja.com" (:network-server . "jabber.aja.com") (:connection-type . starttls)))))
 '(menu-bar-mode nil)
 '(scroll-bar-mode nil)
 '(tool-bar-mode nil nil (tool-bar)))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )

;; Xrefactory configuration part ;;
;; some Xrefactory defaults can be set here
;(defvar xref-current-project nil) ;; can be also "my_project_name"
;(defvar xref-key-binding 'global) ;; can be also 'local or 'none
;(setq load-path (cons "/home/bglover/bin/xref/emacs" load-path))
;(setq exec-path (cons "/home/bglover/bin/xref" exec-path))
;(load "xrefactory")
;; end of Xrefactory configuration part ;;
;;(message "xrefactory loaded")

(message ".emacs loaded")
